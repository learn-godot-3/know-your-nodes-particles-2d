extends Node

func _on_GoToTorchExample_button_up():
	var _scene = get_tree().change_scene("res://Example_Torch.tscn")


func _on_GoToSmokeExample_button_up():
	var _scene = get_tree().change_scene("res://Example_Smoke.tscn")


func _on_GoToTrainExample_button_up():
	var _scene = get_tree().change_scene("res://Example_Train.tscn")


func _on_GoToCoinFountainExample_button_up():
	var _scene = get_tree().change_scene("res://Example_CoinFountain.tscn")


func _on_GoToHyperSpaceExample_button_up():
	var _scene = get_tree().change_scene("res://Example_HyperSpace.tscn")


func _on_GoToSlimeExample_button_up():
	var _scene = get_tree().change_scene("res://Example_Slime.tscn")
