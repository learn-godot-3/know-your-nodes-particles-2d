# Godot Particle System

This is my take on exmaples of the 2D particle system in Godot.
The graphics are part of the series [Know your nodes](https://www.youtube.com/watch?v=awBfTnmgn7k), presented by the Youtube channel [KidsCanCode](https://www.youtube.com/channel/UCNaPQ5uLX5iIEHUCLmfAgKg).

### Menu
![Menu](docs/ex_menu.png)

### Torch
![Torch](docs/ex_torch.png)

### Smoke
![Smoke](docs/ex_smoke.png)

### Train
![Train](docs/ex_train.png)

### Coin Fountain
![Coin Fountain](docs/ex_coin-fountain.png)

### Hyper Space
![Hyper Space](docs/ex_hyper-space.png)

### Slime
![Slime](docs/ex_slime.png)

